### Description ###

This sample project is managing gateways - master devices that control multiple peripheral devices.
The task is to create a REST service (JSON/HTTP) for storing information about these gateways and
their associated devices. 

This information must be stored in the database. When storing a gateway, any field marked as “to be validated” must be validated and an error returned if
it is invalid. Also, no more than 10 peripheral devices are allowed for a gateway.

The service must also offer an operation for displaying information about all stored gateways (and their
devices) and an operation for displaying details for a single gateway. Finally, it must be possible to add
and remove a device from a gateway.

### What is this repository for? ###

Each gateway has:
* a unique serial number (string),
* human-readable name (string),
* IPv4 address (to be validated),
* multiple associated peripheral devices.

Each peripheral device has:
* a UID (number),
* vendor (string),
* date created,
* status - online/offline.

## How do I get set up? ##

* To build & run the application execute 'router.bat' file
* postman collection can be found in data directory

### Endpoints:
* Create gateway:

        POST http://localhost:8080/gateways

        {
            "serialNumber": string // a unique serial number
            "name": "string", //a human-readable name ex: Gateway1
            "ipv4Address": "string" //an IPv4 address ex: 192.168.1.1
        }

* Delete a gateway:
       
       DELETE http://localhost:8080/gateways/{serialNumber}

* Get all stored gateways with device list:

        GET http://localhost:8080/gateways
        GET http://localhost:8080/gateways/optimized  -> this one uses less db query

* Get a single gateway:

        GET http://localhost:8080/gateways/{serialNumber} 

* Add a device to a gateway

        POST http://localhost:8080/gateways/{serialNumber}/devices

        {
            "vendor": "string", // ex: Vendor1
            "status": "online|offline" // ex: online
        }

* Remove a device from a gateway

        DELETE http://localhost:8080/gateways/{serialNumber}/devices/{deviceId}