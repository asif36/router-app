package com.ms.router.repositories;

import com.ms.router.entity.DeviceEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DeviceRepository extends JpaRepository<DeviceEntity, Long> {

    DeviceEntity findByUidAndGateway_Id(Long uid, Long gatewayId);

    List<DeviceEntity> findByGateway_Id(Long gatewayId);

    List<DeviceEntity> findByGateway_IdIn(List<Long> gatewayIdList);

    long countByGateway_Id(Long gatewayId);
}
