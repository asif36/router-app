package com.ms.router.repositories;

import com.ms.router.entity.GatewayEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GatewayRepository extends JpaRepository<GatewayEntity, Long> {
    GatewayEntity findBySerialNumber(String gatewayId);
}
