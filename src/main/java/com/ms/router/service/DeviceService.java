package com.ms.router.service;

import com.ms.router.entity.GatewayEntity;
import com.ms.router.model.Device;

import java.util.List;

public interface DeviceService {
    List<Device> getDevicesForSpecificGateway(GatewayEntity gatewayEntity);

    Device addDevice(GatewayEntity gatewayEntity, Device device);

    void removeDevice(GatewayEntity gatewayEntity, Long uid);

    List<Device> getDeviceListByGatewayIdList(List<Long> gatewayIdList);
}
