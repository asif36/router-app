package com.ms.router.service;

import com.ms.router.entity.GatewayEntity;
import com.ms.router.exception.DuplicateGatewayException;
import com.ms.router.exception.GatewayNotFoundException;
import com.ms.router.model.Device;
import com.ms.router.model.Gateway;
import com.ms.router.repositories.GatewayRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GatewayServiceImpl implements GatewayService {

    private final GatewayRepository gatewayRepository;
    private final DeviceService deviceService;

    public GatewayServiceImpl(GatewayRepository gatewayRepository, DeviceService deviceService) {
        this.gatewayRepository = gatewayRepository;
        this.deviceService = deviceService;
    }

    // TODO: this can be achieved by 2 sql queries depending on business requirement.
    @Override
    public Page<Gateway> getAllGateway(Pageable pageable) {

        Page<GatewayEntity> gatewayEntityPage = gatewayRepository.findAll(pageable);
        List<Gateway> gatewayList = new ArrayList<>();
        for (GatewayEntity gatewayEntity : gatewayEntityPage) {
            gatewayList.add(convertEntityToResource(gatewayEntity));
        }
        return new PageImpl<>(gatewayList, pageable, gatewayEntityPage.getTotalElements());
    }


//    TODO: this is an alternative implementation of previous getAll() method using only 2 sql queries.
    @Override
    public Page<Gateway> getAllGatewayOptimized(Pageable pageable) {
        Page<GatewayEntity> gatewayEntityPage = gatewayRepository.findAll(pageable);
        List<Gateway> gatewayList = new ArrayList<>();
        if(gatewayEntityPage.getTotalElements() == 0){
            return new PageImpl<>(gatewayList, pageable, gatewayEntityPage.getTotalElements());
        }
        List<Long> gatewayIdList = new ArrayList<>();
        Map<String, List<Device>> gatewayDeviceMap = new HashMap<>();
        for(GatewayEntity gatewayEntity: gatewayEntityPage){
            gatewayIdList.add(gatewayEntity.getId());
            List<Device> deviceList = new ArrayList<>();
            gatewayDeviceMap.put(gatewayEntity.getSerialNumber(), deviceList);
        }
        List<Device> deviceList = deviceService.getDeviceListByGatewayIdList(gatewayIdList);

        for(Device device: deviceList){
            List<Device> mappedDeviceList = gatewayDeviceMap.get(device.getGatewaySerialNumber());
            mappedDeviceList.add(device);
            gatewayDeviceMap.put(device.getGatewaySerialNumber(), mappedDeviceList);
        }
        for(GatewayEntity gatewayEntity: gatewayEntityPage){
            Gateway gateway = new Gateway();
            gateway.setSerialNumber(gatewayEntity.getSerialNumber());
            gateway.setName(gatewayEntity.getName());
            gateway.setIpv4Address(gatewayEntity.getIpv4Address());
            gateway.setDeviceList(gatewayDeviceMap.get(gatewayEntity.getSerialNumber()));
            gatewayList.add(gateway);
        }
        return new PageImpl<>(gatewayList, pageable, gatewayEntityPage.getTotalElements());
    }

    @Override
    @Transactional
    public Gateway createGateway(Gateway gateway) {
        GatewayEntity existingEntity = gatewayRepository.findBySerialNumber(gateway.getSerialNumber());
        if (existingEntity != null) {
            throw new DuplicateGatewayException(gateway.getSerialNumber());
        }
        GatewayEntity gatewayEntity = new GatewayEntity();
        gatewayEntity.setSerialNumber(gateway.getSerialNumber());
        gatewayEntity.setName(gateway.getName());
        gatewayEntity.setIpv4Address(gateway.getIpv4Address());
        return convertEntityToResource(gatewayRepository.save(gatewayEntity));
    }

    @Override
    public Gateway getGatewayDetails(String serialNumber) {
        GatewayEntity gatewayEntity = gatewayRepository.findBySerialNumber(serialNumber);
        if (gatewayEntity == null) {
            throw new GatewayNotFoundException(serialNumber);
        }
        return convertEntityToResource(gatewayEntity);
    }

    @Override
    @Transactional
    public Gateway addGatewayDevice(String serialNumber, Device device) {
        GatewayEntity gatewayEntity = gatewayRepository.findBySerialNumber(serialNumber);
        if (gatewayEntity == null) {
            throw new GatewayNotFoundException(serialNumber);
        }
        deviceService.addDevice(gatewayEntity, device);
        return convertEntityToResource(gatewayEntity);
    }

    @Override
    public void removeGatewayDevice(String serialNumber, Long uid) {
        GatewayEntity gatewayEntity = gatewayRepository.findBySerialNumber(serialNumber);
        if (gatewayEntity == null) {
            throw new GatewayNotFoundException(serialNumber);
        }
        deviceService.removeDevice(gatewayEntity, uid);
    }

    private Gateway convertEntityToResource(GatewayEntity gatewayEntity) {
        Gateway gateway = new Gateway();
        gateway.setSerialNumber(gatewayEntity.getSerialNumber());
        gateway.setName(gatewayEntity.getName());
        gateway.setIpv4Address(gatewayEntity.getIpv4Address());
        gateway.setDeviceList(deviceService.getDevicesForSpecificGateway(gatewayEntity));
        return gateway;
    }

}
