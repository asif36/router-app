package com.ms.router.service;

import com.ms.router.model.Device;
import com.ms.router.model.Gateway;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface GatewayService {
    Page<Gateway> getAllGateway(Pageable pageable);
    Page<Gateway> getAllGatewayOptimized(Pageable pageable);

    Gateway createGateway(Gateway gateway);

    Gateway getGatewayDetails(String serialNumber);

    Gateway addGatewayDevice(String serialNumber, Device device);

    void removeGatewayDevice(String serialNumbre, Long uid);

}
