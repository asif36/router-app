package com.ms.router.service;

import com.ms.router.entity.DeviceEntity;
import com.ms.router.entity.GatewayEntity;
import com.ms.router.exception.DeviceLimitExceededException;
import com.ms.router.exception.DeviceNotFoundException;
import com.ms.router.model.Device;
import com.ms.router.model.Status;
import com.ms.router.repositories.DeviceRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class DeviceServiceImpl implements DeviceService {

    private static final Long MAX_DEVICE_LIMIT = 10L;

    private DeviceRepository deviceRepository;

    public DeviceServiceImpl(DeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }

    @Override
    public List<Device> getDevicesForSpecificGateway(GatewayEntity gatewayEntity) {
        List<Device> deviceList = new ArrayList<>();
        List<DeviceEntity> deviceEntityList = deviceRepository.findByGateway_Id(gatewayEntity.getId());
        for (DeviceEntity deviceEntity : deviceEntityList) {
            deviceList.add(convertEntityToResource(deviceEntity));
        }
        return deviceList;
    }

    @Override
    @Transactional
    public Device addDevice(GatewayEntity gatewayEntity, Device device) {
        long deviceCount = deviceRepository.countByGateway_Id(gatewayEntity.getId());
        if (deviceCount == MAX_DEVICE_LIMIT) {
            throw new DeviceLimitExceededException(gatewayEntity.getSerialNumber());
        }
        DeviceEntity deviceEntity = new DeviceEntity();
        deviceEntity.setVendor(device.getVendor());
        deviceEntity.setStatus(Status.valueOf(device.getStatus()));
        deviceEntity.setCreated(new Date());
        deviceEntity.setGateway(gatewayEntity);
        return convertEntityToResource(deviceRepository.save(deviceEntity));
    }

    @Override
    public void removeDevice(GatewayEntity gatewayEntity, Long uid) {
        DeviceEntity deviceEntity = deviceRepository.findByUidAndGateway_Id(uid, gatewayEntity.getId());
        if (deviceEntity == null) {
            throw new DeviceNotFoundException(uid.toString());
        }
        deviceRepository.delete(deviceEntity);
    }

    @Override
    public List<Device> getDeviceListByGatewayIdList(List<Long> gatewayIdList){
        List<DeviceEntity> deviceEntityList = deviceRepository.findByGateway_IdIn(gatewayIdList);
        List<Device> deviceList = new ArrayList<>();
        for(DeviceEntity deviceEntity: deviceEntityList){
            deviceList.add(convertEntityToResource(deviceEntity));
        }
        return deviceList;
    }

    private Device convertEntityToResource(DeviceEntity deviceEntity) {
        Device device = new Device();
        device.setUid(deviceEntity.getUid());
        device.setVendor(deviceEntity.getVendor());
        device.setStatus(deviceEntity.getStatus().getValue());
        device.setCreated(deviceEntity.getCreated());
        device.setGatewaySerialNumber(deviceEntity.getGateway().getSerialNumber());
        return device;
    }

}
