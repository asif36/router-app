package com.ms.router.controller;

import com.ms.router.model.Device;
import com.ms.router.model.Gateway;
import com.ms.router.service.GatewayService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/gateways")
public class GatewayController {

    private GatewayService gatewayService;

    public GatewayController(GatewayService gatewayService) {
        this.gatewayService = gatewayService;
    }

    @GetMapping
    public Page<Gateway> getAllGateways(
            @PageableDefault(size = 10) Pageable pageable) {
        return gatewayService.getAllGateway(pageable);
    }

    @GetMapping("/optimized")
    public Page<Gateway> getAllGatewaysOptimized(
            @PageableDefault(size = 10) Pageable pageable) {
        return gatewayService.getAllGatewayOptimized(pageable);
    }

    @PostMapping
    public ResponseEntity<Gateway> createGateway(@Valid @RequestBody Gateway gateway) {
        return ResponseEntity.ok(gatewayService.createGateway(gateway));
    }

    @PostMapping("/{serialNumber}/devices")
    public ResponseEntity<Gateway> addDevice(@PathVariable String serialNumber, @Valid @RequestBody Device device) {
        return ResponseEntity.ok(gatewayService.addGatewayDevice(serialNumber, device));
    }

    @GetMapping("/{serialNumber}/devices")
    public ResponseEntity<Gateway> getGatewayDetails(@PathVariable String serialNumber) {
        return ResponseEntity.ok(gatewayService.getGatewayDetails(serialNumber));
    }

    @DeleteMapping("/{serialNumber}/devices/{deviceId}")
    public ResponseEntity<Object> removeGatewayDevice(@PathVariable String serialNumber, @PathVariable Long deviceId) {
        gatewayService.removeGatewayDevice(serialNumber, deviceId);
        return ResponseEntity.ok().build();
    }

}
