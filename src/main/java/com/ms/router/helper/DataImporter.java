package com.ms.router.helper;

import com.ms.router.model.Device;
import com.ms.router.model.Gateway;
import com.ms.router.service.GatewayService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@ConditionalOnProperty(prefix = "job.autorun", name = "enabled", havingValue = "true", matchIfMissing = true)
public class DataImporter implements CommandLineRunner {

    private static final String GATEWAY_SERIAL_NO = "12345";
    private static final String GATEWAY_NAME = "Gateway";
    private static final String GATEWAY_IP_ADDRESS = "192.168.1.";

    private static final String DEVICE_VENDOR = "Vendor";
    private static final String DEVICE_STATUS_ONLINE = "online";
    private static final String DEVICE_STATUS_OFFLINE = "offline";

    private static final Logger logger = LoggerFactory.getLogger(DataImporter.class);

    private GatewayService gatewayService;

    public DataImporter(GatewayService gatewayService) {
        this.gatewayService = gatewayService;
    }

    @Override
    public void run(String... args) throws Exception {
        logger.info("Loading data...");

        generateLargeData();

        logger.info("Data import completed.");
    }

    private void generateLargeData() {
        List<String> createdGatewaySerialNumbers = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            Gateway gateway = new Gateway();
            char a = (char) (65 + i);
            String serialNumber = GATEWAY_SERIAL_NO + a;
            gateway.setSerialNumber(serialNumber);
            gateway.setName(GATEWAY_NAME + (i + 1));
            gateway.setIpv4Address(GATEWAY_IP_ADDRESS + i);
            gatewayService.createGateway(gateway);
            createdGatewaySerialNumbers.add(serialNumber);
        }
        for (int i = 1, j = 0; i <= 55; i++) {
            Device device = new Device();
            device.setVendor(DEVICE_VENDOR + i);
            device.setStatus(i % 2 == 0 ? DEVICE_STATUS_OFFLINE : DEVICE_STATUS_ONLINE);

            gatewayService.addGatewayDevice(createdGatewaySerialNumbers.get(j), device);
            if (i % 10 == 0) {
                j++;
            }
        }
    }

}
