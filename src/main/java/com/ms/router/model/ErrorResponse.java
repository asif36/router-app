package com.ms.router.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ErrorResponse {
    private int statusCode;
    private String errorMessage;
}
