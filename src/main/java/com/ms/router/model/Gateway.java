package com.ms.router.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@EqualsAndHashCode
@ToString
public class Gateway implements Serializable {

    private static final String IPV4_ADDRESS_PATTERN = "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

    @NotBlank(message = "serial number can not be empty.")
    @Length(max = 64, message = "serial number should be within 64 characters.")
    private String serialNumber;

    @NotBlank(message = "name can not be empty.")
    @Length(max = 64, message = "name should be within 64 characters.")
    private String name;

    @NotBlank(message = "IPv4 address can not be empty.")
    @Pattern(regexp = IPV4_ADDRESS_PATTERN, message = "invalid IPv4 address.")
    private String ipv4Address;

    private List<Device> deviceList;
}
