package com.ms.router.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@EqualsAndHashCode
@ToString
public class Device implements Serializable {

    private Long uid;

    @NotBlank(message = "Vendor can not be empty.")
    @Length(max = 64, message = "Vendor should be within 64 characters.")
    private String vendor;

    @Pattern(regexp = "^online|offline$", message = "Status value should be 'online' or 'offline'.")
    private String status;

    private Date created;

    private String gatewaySerialNumber;

}
