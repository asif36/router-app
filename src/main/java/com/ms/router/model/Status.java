package com.ms.router.model;

public enum Status {
    online("online"),
    offline("offline");

    private String value;

    Status(String value){
        this.value = value;
    }

    public String getValue(){
        return this.value;
    }

}
