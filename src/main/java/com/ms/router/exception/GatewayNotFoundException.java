package com.ms.router.exception;

public class GatewayNotFoundException extends RuntimeException {

    public GatewayNotFoundException(String serialNumber) {
        super("Could not find gateway: " + serialNumber);
    }
}