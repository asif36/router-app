package com.ms.router.exception;

public class DuplicateGatewayException extends RuntimeException {
    public DuplicateGatewayException(String serialNumber) {
        super("Gateway already exists: " + serialNumber);
    }
}
