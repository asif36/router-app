package com.ms.router.exception;

public class DeviceNotFoundException extends RuntimeException{

    public DeviceNotFoundException(String uid) {
        super("Could not find device: " + uid);
    }

}
