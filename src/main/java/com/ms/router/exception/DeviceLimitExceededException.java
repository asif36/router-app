package com.ms.router.exception;

public class DeviceLimitExceededException extends RuntimeException {
    public DeviceLimitExceededException(String serialNumber) {
        super("Device limit is reached for gateway: " + serialNumber);
    }
}
