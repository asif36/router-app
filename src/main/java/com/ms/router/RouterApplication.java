package com.ms.router;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class RouterApplication {

    public static void main(String[] args) {
        log.info("router application started");
        SpringApplication.run(RouterApplication.class, args);
    }

}
