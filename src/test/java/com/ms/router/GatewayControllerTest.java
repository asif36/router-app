package com.ms.router;

import com.ms.router.exception.DeviceLimitExceededException;
import com.ms.router.exception.DeviceNotFoundException;
import com.ms.router.exception.DuplicateGatewayException;
import com.ms.router.exception.GatewayNotFoundException;
import com.ms.router.helper.RestPageImpl;
import com.ms.router.model.Device;
import com.ms.router.model.ErrorResponse;
import com.ms.router.model.Gateway;
import com.ms.router.service.GatewayService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"job.autorun.enabled=false"}, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GatewayControllerTest {

    @LocalServerPort
    private int port;

    @MockBean
    private GatewayService gatewayService;

    private TestRestTemplate restTemplate;

    private HttpHeaders headers;

    private static final MediaType CONTENT_TYPE = MediaType.APPLICATION_JSON;

    private static final String GATEWAY_SERIAL_NO = "serialNo";
    private static final String GATEWAY_NAME = "gateway1";
    private static final String GATEWAY_IP_ADDRESS = "192.168.1.1";
    private static final Long DEVICE_UID = 1L;
    private static final String DEVICE_VENDOR = "vendor";
    private static final String DEVICE_STATUS_ONLINE = "online";
    private static final String DEVICE_STATUS_OFFLINE = "offline";
    private static final String DEVICE_LIMIT_EXCEEDED_ERROR_MESSAGE = "Device limit";
    private static final String EMPTY_VENDOR_ERROR_MESSAGE = "Vendor can not be empty";

    @Before
    public void setUp() {
        headers = new HttpHeaders();
        headers.setContentType(CONTENT_TYPE);
        restTemplate = new TestRestTemplate();
    }

    @Test
    public void getAllGateways() {
        PageRequest pageRequest = PageRequest.of(0, 1);
        HttpEntity<PageRequest> entity = new HttpEntity<>(pageRequest, headers);

        when(
                gatewayService.getAllGateway(any())
        ).thenReturn(getPaginatedGatewayList());

        ResponseEntity<RestPageImpl<Gateway>> response = restTemplate.exchange(
                createURLWithPort("/gateways"), HttpMethod.GET,
                entity, new ParameterizedTypeReference<RestPageImpl<Gateway>>() {
                }
        );

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(1, response.getBody().getTotalElements());
    }

    @Test
    public void getAllGatewaysOptimized() {
        PageRequest pageRequest = PageRequest.of(0, 1);
        HttpEntity<PageRequest> entity = new HttpEntity<>(pageRequest, headers);

        when(
                gatewayService.getAllGatewayOptimized(any())
        ).thenReturn(getPaginatedGatewayList());

        ResponseEntity<RestPageImpl<Gateway>> response = restTemplate.exchange(
                createURLWithPort("/gateways/optimized"), HttpMethod.GET,
                entity, new ParameterizedTypeReference<RestPageImpl<Gateway>>() {
                }
        );

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(1, response.getBody().getTotalElements());
    }

    @Test
    public void retrieveDetailsForGateway() {
        HttpEntity<Gateway> entity = new HttpEntity<>(null, headers);

        when(
                gatewayService.getGatewayDetails(anyString())
        ).thenReturn(getMockGatewayDetails(false));

        ResponseEntity<Gateway> response = restTemplate.exchange(
                createURLWithPort("/gateways/" + GATEWAY_SERIAL_NO + "/devices"),
                HttpMethod.GET, entity, Gateway.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(GATEWAY_SERIAL_NO, response.getBody().getSerialNumber());
        assertEquals(GATEWAY_NAME, response.getBody().getName());
        assertEquals(GATEWAY_IP_ADDRESS, response.getBody().getIpv4Address());
        assertNotNull(response.getBody().getDeviceList());
        assertEquals(1, response.getBody().getDeviceList().size());
    }

    @Test
    public void retrieveDetailsForGatewayError() {
        HttpEntity<Gateway> entity = new HttpEntity<>(null, headers);

        when(
                gatewayService.getGatewayDetails(Mockito.anyString())
        ).thenThrow(new GatewayNotFoundException(GATEWAY_SERIAL_NO));

        ResponseEntity<ErrorResponse> response = restTemplate.exchange(
                createURLWithPort("/gateways/" + GATEWAY_SERIAL_NO + "/devices"),
                HttpMethod.GET, entity, ErrorResponse.class);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.NOT_FOUND.value(), response.getBody().getStatusCode());
        assertEquals(CONTENT_TYPE, response.getHeaders().getContentType());
        assertTrue(response.getBody().getErrorMessage().contains(GATEWAY_SERIAL_NO));
    }

    @Test
    public void createGateway() {
        Gateway gatewayRequest = new Gateway();
        gatewayRequest.setSerialNumber(GATEWAY_SERIAL_NO);
        gatewayRequest.setName(GATEWAY_NAME);
        gatewayRequest.setIpv4Address(GATEWAY_IP_ADDRESS);

        HttpEntity<Gateway> entity = new HttpEntity<>(gatewayRequest, headers);

        when(
                gatewayService.createGateway(any())
        ).thenReturn(getMockGatewayDetails(true));

        ResponseEntity<Gateway> response = restTemplate.exchange(
                createURLWithPort("/gateways"), HttpMethod.POST, entity, Gateway.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(GATEWAY_SERIAL_NO, response.getBody().getSerialNumber());
        assertEquals(GATEWAY_NAME, response.getBody().getName());
        assertEquals(GATEWAY_IP_ADDRESS, response.getBody().getIpv4Address());
        assertNotNull(response.getBody().getDeviceList());
        assertEquals(0, response.getBody().getDeviceList().size());
    }

    @Test
    public void createGatewayError() {
        Gateway gatewayRequest = new Gateway();
        gatewayRequest.setSerialNumber(GATEWAY_SERIAL_NO);
        gatewayRequest.setName(GATEWAY_NAME);
        gatewayRequest.setIpv4Address(GATEWAY_IP_ADDRESS);

        HttpEntity<Gateway> entity = new HttpEntity<>(gatewayRequest, headers);

        when(
                gatewayService.createGateway(any())
        ).thenThrow(new DuplicateGatewayException(GATEWAY_SERIAL_NO));

        ResponseEntity<ErrorResponse> response = restTemplate.exchange(
                createURLWithPort("/gateways"), HttpMethod.POST, entity, ErrorResponse.class);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getBody().getStatusCode());
        assertEquals(CONTENT_TYPE, response.getHeaders().getContentType());
        assertTrue(response.getBody().getErrorMessage().contains(GATEWAY_SERIAL_NO));
    }

    @Test
    public void addGatewayDevice() {
        Device deviceRequest = new Device();
        deviceRequest.setVendor(DEVICE_VENDOR);
        deviceRequest.setStatus(DEVICE_STATUS_ONLINE);

        HttpEntity<Device> entity = new HttpEntity<>(deviceRequest, headers);

        when(
                gatewayService.addGatewayDevice(anyString(), any())
        ).thenReturn(getMockGatewayDetails(false));

        ResponseEntity<Gateway> response = restTemplate.exchange(
                createURLWithPort("/gateways/" + GATEWAY_SERIAL_NO + "/devices"),
                HttpMethod.POST, entity, Gateway.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
        assertNotNull(response.getBody().getDeviceList());
        assertEquals(1, response.getBody().getDeviceList().size());
        assertEquals(DEVICE_VENDOR, response.getBody().getDeviceList().get(0).getVendor());
        assertEquals(DEVICE_STATUS_ONLINE, response.getBody().getDeviceList().get(0).getStatus());
        assertNotEquals(DEVICE_STATUS_OFFLINE, response.getBody().getDeviceList().get(0).getStatus());
    }

    @Test
    public void addGatewayDeviceError() {
        Device deviceRequest = new Device();
        deviceRequest.setStatus(DEVICE_STATUS_OFFLINE);

        HttpEntity<Device> entity = new HttpEntity<>(deviceRequest, headers);

        when(
                gatewayService.addGatewayDevice(anyString(), any())
        ).thenReturn(getMockGatewayDetails(false));

        ResponseEntity<ErrorResponse> response = restTemplate.exchange(
                createURLWithPort("/gateways/" + GATEWAY_SERIAL_NO + "/devices"),
                HttpMethod.POST, entity, ErrorResponse.class);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getBody().getStatusCode());
        assertEquals(CONTENT_TYPE, response.getHeaders().getContentType());
        assertTrue(response.getBody().getErrorMessage().contains(EMPTY_VENDOR_ERROR_MESSAGE));
    }

    @Test
    public void addGatewayDeviceDeviceLimitExceededError() {
        Device deviceRequest = new Device();
        deviceRequest.setVendor(DEVICE_VENDOR);
        deviceRequest.setStatus(DEVICE_STATUS_OFFLINE);

        HttpEntity<Device> entity = new HttpEntity<>(deviceRequest, headers);

        when(
                gatewayService.addGatewayDevice(anyString(), any())
        ).thenThrow(new DeviceLimitExceededException(DEVICE_UID.toString()));

        ResponseEntity<ErrorResponse> response = restTemplate.exchange(
                createURLWithPort("/gateways/" + GATEWAY_SERIAL_NO + "/devices"),
                HttpMethod.POST, entity, ErrorResponse.class);

        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getBody().getStatusCode());
        assertEquals(CONTENT_TYPE, response.getHeaders().getContentType());
        assertTrue(response.getBody().getErrorMessage().contains(DEVICE_LIMIT_EXCEEDED_ERROR_MESSAGE));
    }

    @Test
    public void removeGatewayDevice() {
        HttpEntity<Device> entity = new HttpEntity<>(null, headers);

        doNothing().when(gatewayService).removeGatewayDevice(anyString(), anyLong());

        ResponseEntity<Object> response = restTemplate.exchange(
                createURLWithPort("/gateways/" + GATEWAY_SERIAL_NO + "/devices/" + DEVICE_UID),
                HttpMethod.DELETE, entity, Object.class);

        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNull(response.getBody());
        verify(gatewayService, times(1)).removeGatewayDevice(anyString(), anyLong());
    }

    @Test
    public void removeGatewayDeviceError() {
        HttpEntity<Device> entity = new HttpEntity<>(null, headers);

        doThrow(new DeviceNotFoundException(DEVICE_UID.toString()))
                .when(gatewayService).removeGatewayDevice(anyString(), anyLong());

        ResponseEntity<Object> response = restTemplate.exchange(
                createURLWithPort("/gateways/" + GATEWAY_SERIAL_NO + "/devices/" + DEVICE_UID),
                HttpMethod.DELETE, entity, Object.class);

        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertNotNull(response.getBody());
        verify(gatewayService, times(1)).removeGatewayDevice(anyString(), anyLong());
    }

    @Test
    public void baseExceptionTest() {
        Gateway gatewayRequest = new Gateway();
        gatewayRequest.setSerialNumber(GATEWAY_SERIAL_NO);
        gatewayRequest.setName(GATEWAY_NAME);
        gatewayRequest.setIpv4Address(GATEWAY_IP_ADDRESS);

        HttpEntity<Gateway> entity = new HttpEntity<>(gatewayRequest, headers);

        when(
                gatewayService.createGateway(any())
        ).thenThrow(NullPointerException.class);

        ResponseEntity<ErrorResponse> response = restTemplate.exchange(
                createURLWithPort("/gateways"), HttpMethod.POST, entity, ErrorResponse.class);

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertNotNull(response.getBody());
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), response.getBody().getStatusCode());
        assertEquals(CONTENT_TYPE, response.getHeaders().getContentType());
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }

    private List<Device> getDeviceList(boolean emptyDeviceList) {
        List<Device> deviceList = new ArrayList<>();
        if (emptyDeviceList) return deviceList;
        Device device1 = new Device();
        device1.setUid(DEVICE_UID);
        device1.setVendor(DEVICE_VENDOR);
        device1.setCreated(new Date());
        device1.setStatus(DEVICE_STATUS_ONLINE);
        deviceList.add(device1);
        return deviceList;
    }

    private Gateway getMockGatewayDetails(boolean emptyDeviceList) {
        Gateway gateway = new Gateway();
        gateway.setSerialNumber(GATEWAY_SERIAL_NO);
        gateway.setName(GATEWAY_NAME);
        gateway.setIpv4Address(GATEWAY_IP_ADDRESS);
        gateway.setDeviceList(getDeviceList(emptyDeviceList));
        return gateway;
    }

    private Page<Gateway> getPaginatedGatewayList() {
        List<Gateway> gatewayList = new ArrayList<>();
        Gateway gateway = new Gateway();
        gateway.setSerialNumber(GATEWAY_SERIAL_NO);
        gateway.setName(GATEWAY_NAME);
        gateway.setIpv4Address(GATEWAY_IP_ADDRESS);
        gatewayList.add(gateway);
        PageRequest pageRequest = PageRequest.of(0, 1);
        return new PageImpl<>(gatewayList, pageRequest, gatewayList.size());
    }
}