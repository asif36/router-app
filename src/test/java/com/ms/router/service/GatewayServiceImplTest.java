package com.ms.router.service;

import com.ms.router.entity.GatewayEntity;
import com.ms.router.exception.DeviceLimitExceededException;
import com.ms.router.exception.DuplicateGatewayException;
import com.ms.router.exception.GatewayNotFoundException;
import com.ms.router.model.Device;
import com.ms.router.model.Gateway;
import com.ms.router.model.Status;
import com.ms.router.repositories.GatewayRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@TestPropertySource(value = "classpath:application-test.properties")
@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"job.autorun.enabled=false"})
public class GatewayServiceImplTest {

    @Autowired
    private GatewayService gatewayService;

    @MockBean
    GatewayRepository gatewayRepository;

    @MockBean
    DeviceService deviceService;

    @TestConfiguration
    static class GatewayServiceImplTestContextConfiguration {
        @Bean
        public GatewayService gatewayService(GatewayRepository gatewayRepository, DeviceService deviceService) {
            return new GatewayServiceImpl(gatewayRepository, deviceService);
        }
    }

    private static final String GATEWAY_SERIAL_NO_1 = "serial1";
    private static final String GATEWAY_NAME_1 = "gateway1";
    private static final String GATEWAY_IP_ADDRESS_1 = "192.168.1.1";
    private static final String DEVICE_1_VENDOR = "Vendor";
    private static final Long DEVICE_1_UID = 12345L;

    @Test
    public void getAllGatewaysTest() {
        PageRequest pageRequest = PageRequest.of(0, 1);
        when(gatewayRepository.findAll(any(PageRequest.class))).thenReturn(getPaginatedGatewayList(false));
        when(deviceService.getDevicesForSpecificGateway(any())).thenReturn(getDeviceList());

        Page<Gateway> gatewayEntityList = gatewayService.getAllGateway(pageRequest);
        Assert.assertNotNull(gatewayEntityList);
        Assert.assertEquals(1, gatewayEntityList.getTotalElements());

        when(gatewayRepository.findAll(any(PageRequest.class))).thenReturn(getPaginatedGatewayList(true));
        Page<Gateway> gatewayEntityList1 = gatewayService.getAllGateway(pageRequest);
        Assert.assertNotNull(gatewayEntityList1);
        Assert.assertEquals(0, gatewayEntityList1.getTotalElements());
    }

    @Test
    public void getAllGatewaysOptimizedTest() {
        PageRequest pageRequest = PageRequest.of(0, 1);
        when(gatewayRepository.findAll(any(PageRequest.class))).thenReturn(getPaginatedGatewayList(false));
        when(deviceService.getDeviceListByGatewayIdList(anyList())).thenReturn(getDeviceList());

        Page<Gateway> gatewayEntityList = gatewayService.getAllGatewayOptimized(pageRequest);
        Assert.assertNotNull(gatewayEntityList);
        Assert.assertEquals(1, gatewayEntityList.getTotalElements());

        when(gatewayRepository.findAll(any(PageRequest.class))).thenReturn(getPaginatedGatewayList(true));
        Page<Gateway> gatewayEntityList1 = gatewayService.getAllGatewayOptimized(pageRequest);
        Assert.assertNotNull(gatewayEntityList1);
        Assert.assertEquals(0, gatewayEntityList1.getTotalElements());
    }

    @Test
    public void getGatewayDetailsTest() {
        when(gatewayRepository.findBySerialNumber(anyString())).thenReturn(this.getMockGatewayEntity());
        when(deviceService.getDevicesForSpecificGateway(any())).thenReturn(this.getDeviceList());

        Gateway gateway = gatewayService.getGatewayDetails(GATEWAY_SERIAL_NO_1);
        Assert.assertNotNull(gateway);
        Assert.assertEquals(GATEWAY_SERIAL_NO_1, gateway.getSerialNumber());
        Assert.assertEquals(GATEWAY_NAME_1, gateway.getName());
        Assert.assertEquals(GATEWAY_IP_ADDRESS_1, gateway.getIpv4Address());
        Assert.assertEquals(2, gateway.getDeviceList().size());
    }

    @Test(expected = GatewayNotFoundException.class)
    public void getGatewayDetailsGatewayNotFoundExceptionTest() {
        when(gatewayRepository.findBySerialNumber(anyString()))
                .thenReturn(null);
        when(deviceService.getDevicesForSpecificGateway(any()))
                .thenReturn(this.getDeviceList());

        gatewayService.getGatewayDetails(GATEWAY_SERIAL_NO_1);
    }

    @Test
    public void createGatewayTest() {
        when(gatewayRepository.save(any())).thenReturn(this.getMockGatewayEntity());
        when(deviceService.getDevicesForSpecificGateway(any())).thenReturn(this.getDeviceList());

        Gateway gatewayRequest = new Gateway();
        gatewayRequest.setName(GATEWAY_SERIAL_NO_1);
        gatewayRequest.setName(GATEWAY_NAME_1);
        gatewayRequest.setIpv4Address(GATEWAY_IP_ADDRESS_1);
        Gateway gateway = gatewayService.createGateway(gatewayRequest);

        Assert.assertNotNull(gateway);
        Assert.assertEquals(GATEWAY_SERIAL_NO_1, gateway.getSerialNumber());
        Assert.assertEquals(GATEWAY_NAME_1, gateway.getName());
        Assert.assertEquals(GATEWAY_IP_ADDRESS_1, gateway.getIpv4Address());
        Assert.assertEquals(2, gateway.getDeviceList().size());
    }

    @Test(expected = DuplicateGatewayException.class)
    public void createGatewayErrorTest() {
        when(gatewayRepository.findBySerialNumber(any())).thenReturn(getMockGatewayEntity());
        when(gatewayRepository.save(any())).thenReturn(getMockGatewayEntity());

        Gateway gatewayRequest = new Gateway();
        gatewayRequest.setName(GATEWAY_SERIAL_NO_1);
        gatewayRequest.setName(GATEWAY_NAME_1);
        gatewayRequest.setIpv4Address(GATEWAY_IP_ADDRESS_1);
        gatewayService.createGateway(gatewayRequest);
    }

    @Test
    public void addGatewayDeviceTest() {
        when(gatewayRepository.findBySerialNumber(anyString())).thenReturn(getMockGatewayEntity());
        when(deviceService.addDevice(any(), any())).thenReturn(getDevice());
        when(deviceService.getDevicesForSpecificGateway(getMockGatewayEntity())).thenReturn(getDeviceList());


        Device deviceRequest = new Device();
        deviceRequest.setVendor(DEVICE_1_VENDOR);
        deviceRequest.setStatus(Status.online.getValue());

        Gateway gateway = gatewayService.addGatewayDevice(GATEWAY_SERIAL_NO_1, deviceRequest);

        Assert.assertNotNull(gateway);
        Assert.assertEquals(GATEWAY_SERIAL_NO_1, gateway.getSerialNumber());
        Assert.assertEquals(GATEWAY_NAME_1, gateway.getName());
        Assert.assertEquals(GATEWAY_IP_ADDRESS_1, gateway.getIpv4Address());
        Assert.assertEquals(2, gateway.getDeviceList().size());
    }

    @Test(expected = DeviceLimitExceededException.class)
    public void addGatewayDeviceDeviceLimitExceededExceptionTest() {
        when(gatewayRepository.findBySerialNumber(anyString())).thenReturn(getMockGatewayEntity());
        when(deviceService.addDevice(any(), any())).thenThrow(DeviceLimitExceededException.class);
        when(deviceService.getDevicesForSpecificGateway(getMockGatewayEntity())).thenReturn(getDeviceList());


        Device deviceRequest = new Device();
        deviceRequest.setVendor(DEVICE_1_VENDOR);
        deviceRequest.setStatus(Status.online.getValue());

        gatewayService.addGatewayDevice(GATEWAY_SERIAL_NO_1, deviceRequest);
    }

    @Test(expected = GatewayNotFoundException.class)
    public void addGatewayDeviceGatewayNotFoundExceptionTest() {
        when(gatewayRepository.findBySerialNumber(anyString())).thenReturn(null);

        Device deviceRequest = new Device();
        deviceRequest.setVendor(DEVICE_1_VENDOR);
        deviceRequest.setStatus(Status.online.getValue());

        gatewayService.addGatewayDevice(GATEWAY_SERIAL_NO_1, deviceRequest);
    }

    @Test
    public void removeGatewayDeviceTest() {
        when(gatewayRepository.findBySerialNumber(anyString())).thenReturn(getMockGatewayEntity());
        doNothing().when(deviceService).removeDevice(any(), anyLong());

        gatewayService.removeGatewayDevice(GATEWAY_SERIAL_NO_1, DEVICE_1_UID);
        verify(deviceService, times(1)).removeDevice(any(), anyLong());
    }

    @Test(expected = GatewayNotFoundException.class)
    public void removeGatewayGatewayNotFoundExceptionTest() {
        when(gatewayRepository.findBySerialNumber(anyString())).thenReturn(null);
        gatewayService.removeGatewayDevice(GATEWAY_SERIAL_NO_1, DEVICE_1_UID);
        verify(gatewayRepository, times(1)).findBySerialNumber(GATEWAY_SERIAL_NO_1);
    }

    private List<Device> getDeviceList() {
        List<Device> deviceList = new ArrayList<>();
        Device device1 = new Device();
        device1.setUid(1L);
        device1.setVendor("vendor1");
        device1.setCreated(new Date());
        device1.setStatus(Status.offline.getValue());
        device1.setGatewaySerialNumber(GATEWAY_SERIAL_NO_1);
        deviceList.add(device1);

        Device device2 = new Device();
        device2.setUid(2L);
        device2.setVendor("vendor2");
        device2.setCreated(new Date());
        device2.setStatus(Status.online.getValue());
        device2.setGatewaySerialNumber(GATEWAY_SERIAL_NO_1);
        deviceList.add(device2);
        return deviceList;
    }

    private GatewayEntity getMockGatewayEntity() {
        GatewayEntity gatewayEntity = new GatewayEntity();
        gatewayEntity.setId(1L);
        gatewayEntity.setSerialNumber(GATEWAY_SERIAL_NO_1);
        gatewayEntity.setName("gateway1");
        gatewayEntity.setIpv4Address("192.168.1.1");
        return gatewayEntity;
    }

    private Device getDevice() {
        Device device = new Device();
        device.setUid(DEVICE_1_UID);
        device.setVendor(DEVICE_1_VENDOR);
        device.setStatus(Status.online.getValue());
        device.setCreated(new Date());
        device.setGatewaySerialNumber(GATEWAY_SERIAL_NO_1);
        return device;
    }

    private Page<GatewayEntity> getPaginatedGatewayList(boolean emptyList) {
        PageRequest pageRequest = PageRequest.of(0, 1);
        List<GatewayEntity> gatewayEntityList = new ArrayList<>();
        if (emptyList) {
            return new PageImpl<>(gatewayEntityList, pageRequest, gatewayEntityList.size());
        }
        GatewayEntity gatewayEntity = new GatewayEntity();
        gatewayEntity.setSerialNumber(GATEWAY_SERIAL_NO_1);
        gatewayEntity.setName(GATEWAY_NAME_1);
        gatewayEntity.setIpv4Address(GATEWAY_IP_ADDRESS_1);
        gatewayEntityList.add(gatewayEntity);
        return new PageImpl<>(gatewayEntityList, pageRequest, gatewayEntityList.size());
    }

}
