package com.ms.router.service;

import com.ms.router.entity.DeviceEntity;
import com.ms.router.entity.GatewayEntity;
import com.ms.router.exception.DeviceLimitExceededException;
import com.ms.router.exception.DeviceNotFoundException;
import com.ms.router.model.Device;
import com.ms.router.model.Status;
import com.ms.router.repositories.DeviceRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@TestPropertySource(value = "classpath:application-test.properties")
@RunWith(SpringRunner.class)
@SpringBootTest(properties = {"job.autorun.enabled=false"})
public class DeviceServiceImplTest {
    @Autowired
    DeviceService deviceService;

    @MockBean
    DeviceRepository deviceRepository;

    private static final String DEVICE_1_VENDOR = "Vendor1";
    private static final Long DEVICE_1_UID = 12345L;
    private static final String DEVICE_1_STATUS = "online";

    @TestConfiguration
    static class DeviceServiceImplTestContextConfiguration {
        @Bean
        public DeviceServiceImpl deviceService(DeviceRepository deviceRepository) {
            return new DeviceServiceImpl(deviceRepository);
        }
    }

    @Test
    public void getDevicesForSpecificGatewayTest() {
        GatewayEntity gatewayEntity = new GatewayEntity();
        when(deviceRepository.findByGateway_Id(any())).thenReturn(this.getDeviceList());

        List<Device> deviceList = deviceService.getDevicesForSpecificGateway(gatewayEntity);
        assertNotNull(deviceList);
        assertEquals(1, deviceList.size());
    }

    @Test
    public void addDeviceTest() {
        GatewayEntity gatewayEntity = new GatewayEntity();
        Device deviceRequest = new Device();
        deviceRequest.setVendor("vendor1");
        deviceRequest.setStatus(Status.offline.getValue());

        when(deviceRepository.countByGateway_Id(any())).thenReturn(0L);
        when(deviceRepository.save(any())).thenReturn(getDeviceEntity());

        Device device = deviceService.addDevice(gatewayEntity, deviceRequest);
        assertNotNull(device);
        assertEquals(DEVICE_1_UID, device.getUid());
        assertEquals(DEVICE_1_VENDOR, device.getVendor());
        assertEquals(DEVICE_1_STATUS, device.getStatus());
    }

    @Test(expected = DeviceLimitExceededException.class)
    public void addDeviceExceptionTest() {
        GatewayEntity gatewayEntity = new GatewayEntity();
        Device deviceRequest = new Device();
        deviceRequest.setVendor("vendor1");
        deviceRequest.setStatus(Status.offline.getValue());

        when(deviceRepository.countByGateway_Id(any())).thenReturn(10L);
        when(deviceRepository.save(any())).thenReturn(getDeviceEntity());

        deviceService.addDevice(gatewayEntity, deviceRequest);
    }

    @Test
    public void removeDeviceTest() {
        GatewayEntity gatewayEntity = new GatewayEntity();

        when(deviceRepository.findByUidAndGateway_Id(anyLong(), any())).thenReturn(getDeviceEntity());
        doNothing().when(deviceRepository).delete(any());

        deviceService.removeDevice(gatewayEntity, 1L);
    }

    @Test(expected = DeviceNotFoundException.class)
    public void removeDeviceExceptionTest() {
        GatewayEntity gatewayEntity = new GatewayEntity();

        when(deviceRepository.findByUidAndGateway_Id(anyLong(), any())).thenReturn(null);
        doNothing().when(deviceRepository).delete(any());

        deviceService.removeDevice(gatewayEntity, 1L);
    }

    @Test
    public void getDeviceListByGatewayIdListTest(){
        List<Long> gatewayIdList = new ArrayList<>();
        gatewayIdList.add(1L);

        when(deviceRepository.findByGateway_IdIn(anyList())).thenReturn(getDeviceList());
        List<Device> deviceList = deviceService.getDeviceListByGatewayIdList(gatewayIdList);
        assertNotNull(deviceList);
        assertEquals(1, deviceList.size());

        when(deviceRepository.findByGateway_IdIn(anyList())).thenReturn(new ArrayList<>());
        deviceList = deviceService.getDeviceListByGatewayIdList(gatewayIdList);
        assertNotNull(deviceList);
        assertEquals(0, deviceList.size());
    }


    private List<DeviceEntity> getDeviceList() {
        List<DeviceEntity> deviceList = new ArrayList<>();
        deviceList.add(getDeviceEntity());
        return deviceList;
    }

    private DeviceEntity getDeviceEntity() {
        DeviceEntity deviceEntity = new DeviceEntity();
        deviceEntity.setUid(DEVICE_1_UID);
        deviceEntity.setVendor(DEVICE_1_VENDOR);
        deviceEntity.setCreated(new Date());
        deviceEntity.setStatus(Status.online);
        deviceEntity.setGateway(getGatewayEntity());
        return deviceEntity;
    }

    private GatewayEntity getGatewayEntity() {
        GatewayEntity gatewayEntity = new GatewayEntity();
        gatewayEntity.setId(1L);
        gatewayEntity.setSerialNumber("12345D");
        gatewayEntity.setName("AB");
        gatewayEntity.setName("123.256.12.10");
        return gatewayEntity;
    }

}
