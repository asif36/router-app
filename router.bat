@echo off
set message=building router application
echo %message%
call mvn clean
call mvn package
set message=application build complete
echo %message%
REM %JAVA_HOME%
java -jar target/router-app-1.0.0-SNAPSHOT.jar
PAUSE